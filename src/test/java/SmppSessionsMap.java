/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author eatakishiyev
 */
public class SmppSessionsMap extends ConcurrentHashMap<String, List<Session>> {

    public SmppSessionsMap() {
        super();
    }

    public List<Session> get(String systemId) {
        return super.get(systemId);
    }

    public Session get(String systemId, long sessionId) {
        List<Session> sessions = super.get(systemId);
        if (sessions == null) {
            return null;
        }

        for (Session session : sessions) {
            if (session.getSessionId() == sessionId) {
                return session;
            }
        }
        return null;
    }

    public void removeSession(String systemId, long sessionId) {
        List<Session> sessions = super.get(systemId);
        if (sessions == null) {
            return;
        }
        for (Session session : sessions) {
            if (session.getSessionId() == sessionId) {
                if (session.enquireLinkTask != null) {
                    session.enquireLinkTask.cancel(true);
                }
                sessions.remove(session);

                if (sessions.size() <= 0) {
                    super.remove(systemId);
                }

                return;
            }
        }
    }
}
