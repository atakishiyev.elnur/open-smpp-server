/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppServerSession;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import com.cloudhopper.smpp.type.RecoverablePduException;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.SmppTimeoutException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;
import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class Session {

    private static final Logger LOGGER = Logger.getLogger(Session.class);
    private long sessionId;
    private SmppServerSession smppServerSession;
    private final AtomicInteger sequenceId = new AtomicInteger(0);
    protected ScheduledFuture<?> enquireLinkTask;

    protected Session(long Id, SmppServerSession smppServerSession) {
        this.sessionId = Id;
        this.smppServerSession = smppServerSession;
    }

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public void setSmppServerSession(SmppServerSession smppServerSession) {
        this.smppServerSession = smppServerSession;
    }

    public int fetchSequenceId() {
        return this.sequenceId.incrementAndGet();
    }

    public String getHost() {
        return this.smppServerSession.getConfiguration().getHost();
    }

    public int getPort() {
        return this.smppServerSession.getConfiguration().getPort();
    }

    public void sendResponsePdu(PduResponse response) throws RecoverablePduException, UnrecoverablePduException, SmppChannelException, InterruptedException {
        this.smppServerSession.sendResponsePdu(response);
    }

    public void sendRequestPdu(PduRequest request, long timeOut, boolean synchr) throws RecoverablePduException, UnrecoverablePduException, SmppTimeoutException, SmppChannelException, InterruptedException {
        this.smppServerSession.sendRequestPdu(request, timeOut, synchr);
    }

    public void destroy() {
        this.smppServerSession.destroy();
    }

    public void close() {
        this.smppServerSession.close();
    }

    public SmppBindType getBindType() {
        return this.smppServerSession.getBindType();
    }

    public boolean isBound() {
        return this.smppServerSession.isBound();
    }

    public void unbind(long timeout) {
        this.smppServerSession.unbind(timeout);
    }

    public SmppServerSession getSmppServerSession() {
        return smppServerSession;
    }

    public String createMessageId() {
        StringBuilder sb = new StringBuilder();
        sb.append(System.nanoTime());
        sb.append(new Random().nextLong());
        return sb.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other instanceof Session) {
            return ((Session) other).getSessionId() == this.sessionId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (int) (this.sessionId ^ (this.sessionId >>> 32));
        return hash;
    }

}
