/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server;

/**
 *
 * @author eatakishiyev
 */
public interface SmppServerMBean{

    /**
     * @return the bindTimeOut
     */
    public long getBindTimeOut();

    /**
     * @return the defaultRequestExpiryTimeOut
     */
    public long getRequestExpiryTimeOut();

    /**
     * @return the defaultWindowMonitorInterval
     */
    public long getWindowMonitorInterval();

    /**
     * @return the defaultWindowSize
     */
    public int getWindowSize();

    /**
     * @return the defaultWindowWaitTimeOut
     */
    public long getWindowWaitTimeOut();

    /**
     * @return the maxConnectionSize
     */
    public int getMaxConnectionSize();

    /**
     * @return the port
     */
    public int getPort();

    /**
     * @param bindTimeOut the bindTimeOut to set
     */
    public void setBindTimeOut(long bindTimeOut);

    /**
     * @param defaultRequestExpiryTimeOut the defaultRequestExpiryTimeOut to set
     */
    public void setRequestExpiryTimeOut(long defaultRequestExpiryTimeOut);

    /**
     * @param defaultWindowMonitorInterval the defaultWindowMonitorInterval to
     * set
     */
    public void setWindowMonitorInterval(long defaultWindowMonitorInterval);

    /**
     * @param defaultWindowSize the defaultWindowSize to set
     */
    public void setWindowSize(int defaultWindowSize);

    /**
     * @param defaultWindowWaitTimeOut the defaultWindowWaitTimeOut to set 
     */
    public void setWindowWaitTimeOut(long defaultWindowWaitTimeOut);

    /**
     * @param maxConnectionSize the maxConnectionSize to set
     */
    public void setMaxConnectionSize(int maxConnectionSize);

    /**
     * @param port the port to set
     */
    public void setPort(int port);

    public void start() throws Exception;

    public void stop();
    
    public void restart() throws Exception;

}
