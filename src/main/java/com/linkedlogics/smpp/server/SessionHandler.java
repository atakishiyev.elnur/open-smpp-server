/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server;

import com.cloudhopper.smpp.PduAsyncResponse;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppServerSession;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import javax.naming.NamingException;
import com.linkedlogics.smpp.server.esme.EsmeImpl;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class SessionHandler extends DefaultSmppSessionHandler {

    private final SmppServerSession serverSession;
    private final Logger logger = Logger.getLogger(SessionHandler.class);
    private long sessionId;
    private final String systemId;
    private final AtomicLong uniqueSequenceIdGenerator;
    private final ServerHandler serverHandler;
    private ScheduledFuture<?> inactivityMonitorTask;
    protected ScheduledFuture<?> enquireLinkTask;
    private final ScheduledExecutorService scheduler;
    private final EsmeImpl esme;
    private long last_activity;

    public SessionHandler(SmppServerSession serverSession, ServerHandler serverHandler,
            String systemId, AtomicLong uniqueSequenceIdGenerator,
            ScheduledExecutorService scheduler, EsmeImpl esme) throws NamingException {
        this.serverSession = serverSession;
        this.systemId = systemId;
        this.uniqueSequenceIdGenerator = uniqueSequenceIdGenerator;
        this.scheduler = scheduler;
        this.esme = esme;
        this.serverHandler = serverHandler;
    }

    @Override
    public void fireExpectedPduResponseReceived(PduAsyncResponse pduAsyncResponse) {
        int commandId = pduAsyncResponse.getResponse().getCommandId();

        logger.info("ExpectedPduResponseReceived");

        switch (commandId) {
            case SmppConstants.CMD_ID_DELIVER_SM_RESP:
                serverHandler.getSmppUser().onDeliverSmResponse(pduAsyncResponse, this);
                break;
            case SmppConstants.CMD_ID_DATA_SM_RESP:
                serverHandler.getSmppUser().onDataSmResponse(pduAsyncResponse, this);
                break;
        }
    }

    @Override
    public void firePduRequestExpired(PduRequest pduRequest) {
        serverHandler.getSmppUser().onRequestExpired(pduRequest, this);
    }

    @Override
    public PduResponse firePduRequestReceived(PduRequest pduRequest) {
        long uniqueSequenceId = this.uniqueSequenceIdGenerator.getAndIncrement();
        pduRequest.setReferenceObject(uniqueSequenceId);

        switch (pduRequest.getCommandId()) {
            case SmppConstants.CMD_ID_SUBMIT_SM:
                if (esme.doThrootling()) {
                    logger.error("Esme throotling triggered" + esme);
                    return pduRequest.createGenericNack(SmppConstants.STATUS_MSGQFUL);
                }

                serverHandler.getSmppUser().onSubmitSmRequest(pduRequest, this);
                last_activity = System.currentTimeMillis();
                break;

            case SmppConstants.CMD_ID_DATA_SM:
                if (esme.doThrootling()) {
                    logger.error("Esme throotling triggered" + esme);
                    return pduRequest.createGenericNack(SmppConstants.STATUS_MSGQFUL);
                }
                serverHandler.getSmppUser().onDataSmRequest(pduRequest, this);
                last_activity = System.currentTimeMillis();
                break;
            case SmppConstants.CMD_ID_ENQUIRE_LINK:
                last_activity = System.currentTimeMillis();
                return pduRequest.createResponse();
            default:
                return pduRequest.createResponse();
        }

        return null;
    }

    public void fireConnected(int id, Long sessionId) {
        serverHandler.getSmppUser().onConnected(systemId, id, sessionId, this);
    }

    protected void scheduleInactivityMonitor() {
        this.inactivityMonitorTask = scheduler.schedule(new InactivityMonitorThread(), esme.getInactivityMonitorTimer(), TimeUnit.SECONDS);
    }

    protected void unscheduleInactivityMonitor() {
        if (this.inactivityMonitorTask != null) {
            this.inactivityMonitorTask.cancel(true);
        }
    }

    protected class InactivityMonitorThread implements Runnable {

        @Override
        public void run() {
            long current_time = System.currentTimeMillis();
            if (current_time - SessionHandler.this.last_activity > esme.getInactivityMonitorTimer()) {
                logger.info("Inactivity detected on " + systemId + "during " + esme.getInactivityMonitorTimer() + " seconds. Connection dropped.");
                SessionHandler.this.esme.disconnect();
                SessionHandler.this.unscheduleInactivityMonitor();
            }
        }
    }

    public long getSessionId() {
        return sessionId;
    }

    public SmppServerSession getServerSession() {
        return serverSession;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }

        if (other instanceof SessionHandler) {
            return ((SessionHandler) other).getSessionId() == this.sessionId;
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int) (this.sessionId ^ (this.sessionId >>> 32));
        return hash;
    }

}
