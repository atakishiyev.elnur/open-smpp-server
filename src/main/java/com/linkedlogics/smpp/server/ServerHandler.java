/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppServerHandler;
import com.cloudhopper.smpp.SmppServerSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.BaseBind;
import com.cloudhopper.smpp.pdu.BaseBindResp;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.type.SmppProcessingException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import com.linkedlogics.smpp.server.esme.EsmeImpl;
import com.linkedlogics.smpp.server.esme.EsmeManagement;
import com.linkedlogics.smpp.server.esme.EsmeManagementMBean;
import java.rmi.RemoteException;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class ServerHandler implements SmppServerHandler {

    private final static Logger LOGGER = Logger.getLogger(ServerHandler.class);
    private EsmeManagementMBean esmeManagement;
    private final AtomicLong uniqueSequenceNumberGenerator;
    private final ScheduledExecutorService enquireLinkScheduler;
    private final ScheduledExecutorService sessionActivityMonitor;
    private SmppUser smppUser;

    public ServerHandler() {
        try {
            this.esmeManagement = new EsmeManagement();
        } catch (RemoteException ex) {
            LOGGER.error("ErrorOccured during initialize smpp server handler. ", ex);
        }
        this.uniqueSequenceNumberGenerator = new AtomicLong();
        this.enquireLinkScheduler = Executors.newScheduledThreadPool(5, new ThreadFactory() {

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "SmppEnquiryLinkMonitor");
            }
        });
        this.sessionActivityMonitor = Executors.newScheduledThreadPool(5, new ThreadFactory() {

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "SmppSessionActivityMonitor");
            }
        });
    }

    @Override
    public void sessionBindRequested(Long l, SmppSessionConfiguration smppSessionConfiguration, BaseBind baseBind) throws SmppProcessingException {
    }

    @Override
    public void sessionCreated(Long sessionId, SmppServerSession smppServerSession, BaseBindResp baseBindResp) throws SmppProcessingException {
        try {
            String systemId = smppServerSession.getConfiguration().getSystemId();
            EsmeImpl esme = this.getEsmeManagement().getEsme(systemId);

            //Esme not configured
            if (esme == null) {
                baseBindResp.setCommandStatus(SmppConstants.STATUS_INVSYSID);
                smppServerSession.sendResponsePdu(baseBindResp);
                smppServerSession.close();
                smppServerSession.destroy();
                throw new SmppProcessingException(SmppConstants.STATUS_INVSYSID);
            }

            //Esme configured but disabled
            if (esme.isDisabled()) {
                baseBindResp.setCommandStatus(SmppConstants.STATUS_INVSYSID);
                smppServerSession.sendResponsePdu(baseBindResp);
                smppServerSession.close();
                smppServerSession.destroy();
                throw new SmppProcessingException(SmppConstants.STATUS_INVSYSID);
            }

            //Access controlling on base of source host
            if (esme.getHost() != null
                    && !esme.getHost().trim().equals("")) {
                if (!esme.getHost().equals(smppServerSession.getConfiguration().getHost())) {
                    baseBindResp.setCommandStatus(SmppConstants.STATUS_UNKNOWNERR);
                    smppServerSession.sendResponsePdu(baseBindResp);
                    smppServerSession.close();
                    smppServerSession.destroy();
                    throw new SmppProcessingException(SmppConstants.STATUS_INVSYSID);
                }
            }

            //Incorrect password
            if (!esme.getPassword().equals(smppServerSession.getConfiguration().getPassword())) {
                baseBindResp.setCommandStatus(SmppConstants.STATUS_INVPASWD);
                smppServerSession.sendResponsePdu(baseBindResp);
                smppServerSession.close();
                smppServerSession.destroy();
                throw new SmppProcessingException(SmppConstants.STATUS_INVPASWD);
            }

            //Mismatch bind type with configured one
            if (esme.getBindType() != null
                    && smppServerSession.getBindType() != esme.getBindType()) {
                baseBindResp.setCommandStatus(SmppConstants.STATUS_INVBNDSTS);
                smppServerSession.sendResponsePdu(baseBindResp);
                smppServerSession.close();
                smppServerSession.destroy();
                throw new SmppProcessingException(SmppConstants.STATUS_INVBNDSTS);
            }

            //Max count of concurrent sessions for ESME reached
            if (esme.getSession() != null
                    || esme.getSession().getServerSession().isBound()) {
                baseBindResp.setCommandStatus(SmppConstants.STATUS_ALYBND);
                smppServerSession.sendResponsePdu(baseBindResp);
                smppServerSession.close();
                smppServerSession.destroy();
                throw new SmppProcessingException(SmppConstants.STATUS_ALYBND);
            }

            if (esme.getWindowSize() != null && esme.getWindowSize() > 0) {
                smppServerSession.getConfiguration().setWindowSize(esme.getWindowSize());
            }

            if (esme.getWindowTimeOut() != null) {
                smppServerSession.getConfiguration().setWindowWaitTimeout(esme.getWindowTimeOut());
            }

            if (esme.getWindowMonitorInterval() != null) {
                smppServerSession.getConfiguration().setWindowMonitorInterval(esme.getWindowMonitorInterval());
            }

            SessionHandler sessionHandler = new SessionHandler(smppServerSession,
                    this, systemId, uniqueSequenceNumberGenerator,
                    this.sessionActivityMonitor, esme);

            if (esme.getEnquiryLinkTimer() != null
                    && esme.getEnquiryLinkTimer() > 0) {
                if (smppServerSession.getBindType() == SmppBindType.TRANSCEIVER || smppServerSession.getBindType() == SmppBindType.RECEIVER) {
                    sessionHandler.enquireLinkTask
                            = enquireLinkScheduler.scheduleAtFixedRate(new EnquireLinkTask(esme, sessionHandler, esme.getEnquiryRespondTimer()),
                                    esme.getEnquiryLinkTimer(), esme.getEnquiryLinkTimer(), TimeUnit.SECONDS);
                } else {
                    LOGGER.error("Cannot start enquiry link task for " + esme.getSystemId() + ". BindType is " + smppServerSession.getBindType());
                }
            }

            if (esme.getInactivityMonitorTimer() != null
                    && esme.getInactivityMonitorTimer() > 0) {
                if (smppServerSession.getBindType() == SmppBindType.TRANSCEIVER
                        || smppServerSession.getBindType() == SmppBindType.TRANSMITTER) {
                    sessionHandler.scheduleInactivityMonitor();
                }
            }
            smppServerSession.serverReady(sessionHandler);

            sessionHandler.fireConnected(new Random().nextInt(), sessionId);
        } catch (Exception ex) {
            LOGGER.error("Error occured ", ex);
            smppServerSession.close();
        }
    }

    @Override
    public void sessionDestroyed(Long sessionId, SmppServerSession smppServerSession) {
        EsmeImpl esme = esmeManagement.getEsme(smppServerSession.getConfiguration().getSystemId());
        esme.disconnect();
    }

    /**
     * @return the esmeManagement
     */
    public EsmeManagementMBean getEsmeManagement() {
        return esmeManagement;
    }

    public void setSmppUser(SmppUser smppUser) {
        this.smppUser = smppUser;
    }

    public SmppUser getSmppUser() {
        return smppUser;
    }
}
