/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server;

import com.cloudhopper.smpp.pdu.EnquireLink;
import com.linkedlogics.smpp.server.esme.EsmeImpl;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class EnquireLinkTask implements Runnable {

    private final SessionHandler sessionHandler;
    private final static Logger LOGGER = Logger.getLogger(EnquireLinkTask.class);
    private final EsmeImpl esme;
    private final Long enquiryRespondTimer;

    public EnquireLinkTask(EsmeImpl esme, SessionHandler sessionHandler, Long enquiryRespondTimer) {
        this.esme = esme;
        this.sessionHandler = sessionHandler;
        this.enquiryRespondTimer = enquiryRespondTimer;
    }

    @Override
    public void run() {
        try {
            if (enquiryRespondTimer == null || enquiryRespondTimer < 0) {
                sessionHandler.getServerSession().enquireLink(new EnquireLink(), 1000);
            } else {
                sessionHandler.getServerSession().enquireLink(new EnquireLink(), enquiryRespondTimer);
            }
        } catch (Exception ex) {
            LOGGER.error("", ex);
            esme.disconnect();
        }

    }

}
