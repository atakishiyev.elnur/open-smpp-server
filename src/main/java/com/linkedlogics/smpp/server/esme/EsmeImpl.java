/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server.esme;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.linkedlogics.smpp.server.SessionHandler;
import java.util.Random;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class EsmeImpl {

    private final Random randomizer = new Random();
    private int maxConnection = 1;
    private String systemId;
    private String password;
    private SmppBindType bindType;
    private Long enquiryLinkTimer;
    private String host;
    private String stateName;
    private Integer windowSize = 30;
    private boolean disabled = false;
    private Long windowMonitorInterval = null;
    private Long windowTimeOut = null;
    private Long enquiryRespondTimer = null;
    private Long inactivityMonitorTimer = null;
    private String ip;
    private int port;
    private int tps;
    private long startTime;
    private int currentTps = 0;
    private SessionHandler session;

    private final Logger logger = Logger.getLogger(EsmeImpl.class);

    public EsmeImpl(String systemId, String password, int maxConnection) {
        this.systemId = systemId;
        this.password = password;
        this.maxConnection = maxConnection;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the systemId
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the bindType
     */
    public SmppBindType getBindType() {
        return bindType;
    }

    /**
     * @param bindType the bindType to set
     */
    public void setBindType(SmppBindType bindType) {
        this.bindType = bindType;
    }

    /**
     * @return the enquiryLinkTimer
     */
    public Long getEnquiryLinkTimer() {
        return enquiryLinkTimer;
    }

    /**
     * @param enquiryLinkTimer the enquiryLinkTimer to set
     */
    public void setEnquiryLinkTimer(Long enquiryLinkTimer) {
        this.enquiryLinkTimer = enquiryLinkTimer;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * @param stateName the stateName to set
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * @return the windowSize
     */
    public Integer getWindowSize() {
        return windowSize;
    }

    /**
     * @param windowSize the windowSize to set
     */
    public void setWindowSize(Integer windowSize) {
        this.windowSize = windowSize;
    }

    public int getTps() {
        return tps;
    }

    public void setTps(int tps) {
        this.tps = tps;
    }

    public void disable() {
        this.disabled = true;
    }

    public void enable() {
        this.disabled = false;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SystemId = ").append(this.systemId).append("\n");
        sb.append(String.format("BindType = %s\n", this.bindType));
        sb.append(String.format("AllowedHosts = %s\n", this.host));
        sb.append(String.format("EnquiryLinkTimer = %s\n", this.enquiryLinkTimer));
        sb.append(String.format("WindowSize = %s\n", this.windowSize));
        sb.append(String.format("State = %s\n", this.stateName));
        sb.append(String.format("Disabled = %s\n", this.disabled));
        sb.append(String.format("MaxConnections = %s\n", this.maxConnection));
        sb.append(String.format("WindowWaitTimeOut = %s\n", this.windowTimeOut));
        sb.append(String.format("WindowMonitorInteval = %s\n", this.windowMonitorInterval));
        sb.append(String.format("TPS = %s\n", this.tps));
        return sb.toString();
    }

    /**
     * @return the maxConnection
     */
    public int getMaxConnection() {
        return maxConnection;
    }

    /**
     * @param maxConnection the maxConnection to set
     */
    public void setMaxConnection(int maxConnection) {
        this.maxConnection = maxConnection;
    }

    public Long getWindowTimeOut() {
        return this.windowTimeOut;
    }

    public Long getWindowMonitorInterval() {
        return this.windowMonitorInterval;
    }

    /**
     * @param windowTimeOut the windowTimeOut to set
     */
    public void setWindowTimeOut(Long windowTimeOut) {
        this.windowTimeOut = windowTimeOut;
    }

    /**
     * @param windowMonitorInterval the windowMonitorInterval to set
     */
    public void setWindowMonitorInterval(Long windowMonitorInterval) {
        this.windowMonitorInterval = windowMonitorInterval;
    }

    /**
     * @return the enquiryRespondTimer
     */
    public Long getEnquiryRespondTimer() {
        return enquiryRespondTimer;
    }

    /**
     * @param enquiryRespondTimer the enquiryRespondTimer to set
     */
    public void setEnquiryRespondTimer(Long enquiryRespondTimer) {
        this.enquiryRespondTimer = enquiryRespondTimer;
    }

    /**
     * @return the inactivityMonitor
     */
    public Long getInactivityMonitorTimer() {
        return inactivityMonitorTimer;
    }

    /**
     * @param inactivityMonitorTimer
     */
    public void setInactivityMonitorTimer(Long inactivityMonitorTimer) {
        this.inactivityMonitorTimer = inactivityMonitorTimer;
    }

    public boolean doThrootling() {
        if (System.currentTimeMillis() - startTime >= 1000) {
            startTime = System.currentTimeMillis();
            currentTps = 0;
        }

        currentTps++;
        if (currentTps > tps) {
            logger.error("Esme throotling triggered" + currentTps + " Esme " + toString());
            return true;
        }
        return false;
    }

    public SessionHandler getSession() {
        return session;
    }

    public void disconnect() {
        session.getServerSession().unbind(1000);
        session.getServerSession().destroy();
        session.getServerSession().close();
    }

    public void send(PduRequest request) throws Exception {
        session.getServerSession().sendRequestPdu(request, 0, false);
    }
}
