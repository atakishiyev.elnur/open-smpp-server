/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server.esme;

/**
 *
 * @author eatakishiyev
 */
public interface EsmeManagementMBean {

    public EsmeImpl addClientEsme(String systemId, String password, String host,
            String bindType, Integer windowSize, Integer maxConnection) throws Exception;

    public EsmeImpl getEsme(String systemId);

    public void removeEsme(String systemId) throws Exception;

    public void storeConfig() throws Exception;

    public void loadConfig() throws Exception;

    public String displayConfig(String esme) throws Exception;

    public void disableEsme(String esme) throws Exception;

    public void enableEsme(String esme) throws Exception;

    public void disconnectEsme(String esm) throws Exception;
}
