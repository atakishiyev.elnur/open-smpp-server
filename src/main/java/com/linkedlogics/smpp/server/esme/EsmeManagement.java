/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server.esme;

import com.cloudhopper.smpp.SmppBindType;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


/**
 *
 * @author eatakishiyev
 */
public class EsmeManagement implements EsmeManagementMBean {

    private final String configPath = "./conf/esme.xml";
    private final List<EsmeImpl> configuredEsme = new ArrayList<>();
    private final Logger logger = Logger.getLogger(EsmeManagement.class);

    public EsmeManagement() throws RemoteException {
        try {
            this.loadConfig();
        } catch (JDOMException | IOException ex) {
            logger.error("Error occured while instantiate EsmeManagement", ex);
        }
    }

    @Override
    public EsmeImpl addClientEsme(String systemId, String password, String host,
            String bindType, Integer windowSize, Integer maxConnection) throws Exception {
        for (EsmeImpl esme : configuredEsme) {
            if (esme.getSystemId().equals(systemId)) {
                throw new IOException(String.format("Esme already exists. SystemId = %s", systemId));
            }
        }

        EsmeImpl clientEsme = new EsmeImpl(systemId, password, maxConnection);
        if (host != null && !host.trim().equals("")) {
            clientEsme.setHost(host);
        }

        if (bindType != null && !bindType.trim().equals("")) {
            clientEsme.setBindType(SmppBindType.valueOf(bindType));
        }

        if (windowSize != null && windowSize > 0) {
            clientEsme.setWindowSize(windowSize);
        }

        this.configuredEsme.add(clientEsme);
        return clientEsme;
    }

    @Override
    public void removeEsme(String systemId) throws Exception {
        for (EsmeImpl esme : configuredEsme) {
            if (esme.getSystemId().equals(systemId)) {
                this.configuredEsme.remove(esme);
                return;
            }
        }
        throw new IOException(String.format("Esme not exists. SystemId = %s", systemId));
    }

    @Override
    public EsmeImpl getEsme(String systemId) {
        if (systemId == null || systemId.trim().isEmpty()) {
            return null;
        }

        for (EsmeImpl esme : configuredEsme) {
            EsmeImpl clientEsme = (EsmeImpl) esme;
            if (clientEsme.getSystemId().equals(systemId)) {
                return clientEsme;
            }
        }
        return null;
    }

    @Override
    public void storeConfig() throws IOException {
        Element rootElement = new Element("esmeList");

        for (EsmeImpl esme : this.configuredEsme) {
            Element esmeElement = new Element("esme");
            esmeElement.setAttribute("systemId", esme.getSystemId()).
                    setAttribute("password", esme.getPassword());

            if (esme.getHost() != null && !esme.getHost().trim().equals("")) {
                esmeElement.setAttribute("host", esme.getHost());
            }

            if (esme.getBindType() != null) {
                esmeElement.setAttribute("bindType", esme.getBindType().name());
            }

            if (esme.getEnquiryLinkTimer() != null && esme.getEnquiryLinkTimer() > 0) {
                esmeElement.setAttribute("enquiryLinkTimer", String.valueOf(esme.getEnquiryLinkTimer()));
            }

            if (esme.getEnquiryRespondTimer() != null && esme.getEnquiryRespondTimer() > 0) {
                esmeElement.setAttribute("enquiryRespondTimer", String.valueOf(esme.getEnquiryRespondTimer()));
            }

            if (esme.getWindowSize() != null && esme.getWindowSize() > 0) {
                esmeElement.setAttribute("windowsize", String.valueOf(esme.getWindowSize()));
            }

            esmeElement.setAttribute("maxConnection", String.valueOf(esme.getMaxConnection()));

            if (esme.getWindowMonitorInterval() != null) {
                esmeElement.setAttribute("windowMonitorInterval", String.valueOf(esme.getWindowMonitorInterval()));
            }

            if (esme.getWindowTimeOut() != null) {
                esmeElement.setAttribute("windowTimeOut", String.valueOf(esme.getWindowTimeOut()));
            }

            if (esme.getInactivityMonitorTimer() != null) {
                esmeElement.setAttribute("inactivityMonitorTimer", String.valueOf(esme.getInactivityMonitorTimer()));
            }

            rootElement.addContent(esmeElement);
        }

        Document document = new Document(rootElement);
        XMLOutputter xMLOutputter = new XMLOutputter(Format.getPrettyFormat());

        xMLOutputter.output(document, new FileOutputStream(this.configPath));
    }

    @Override
    public final void loadConfig() throws FileNotFoundException, JDOMException, IOException {

        logger.info("Loading Esmes from configuration");
        SAXBuilder sAXBuilder = new SAXBuilder();

        Document document = sAXBuilder.build(new FileInputStream(configPath));
        Element rootElement = document.getRootElement();
        List<Element> esmeList = rootElement.getChildren("esme");
        this.configuredEsme.clear();
        for (Element esme : esmeList) {
            EsmeImpl esmeImpl = new EsmeImpl(esme.getAttributeValue("systemId"),
                    esme.getAttributeValue("password"),
                    esme.getAttribute("maxConnection").getIntValue());

            String bindType = esme.getAttributeValue("bindType");
            if (bindType != null && !bindType.trim().equals("")) {
                esmeImpl.setBindType(SmppBindType.valueOf(bindType));
            }

            String host = esme.getAttributeValue("host");
            if (host != null && !host.trim().equals("")) {
                esmeImpl.setHost(host);
            }

            Attribute enquiryLinkTimer = esme.getAttribute("enquiryLinkTimer");
            if (enquiryLinkTimer != null) {
                esmeImpl.setEnquiryLinkTimer(enquiryLinkTimer.getLongValue());
            }

            Attribute enquiryRespondTimer = esme.getAttribute("enquiryRespondTimer");
            if (enquiryRespondTimer != null) {
                esmeImpl.setEnquiryRespondTimer(enquiryRespondTimer.getLongValue());
            }

            String windowSize = esme.getAttributeValue("windowsize");
            if (windowSize != null && !windowSize.trim().equals("")) {
                esmeImpl.setWindowSize(Integer.parseInt(windowSize));
            }

            String windowTimeOut = esme.getAttributeValue("windowTimeOut");
            if (windowTimeOut != null && !windowTimeOut.trim().equals("")) {
                esmeImpl.setWindowTimeOut(Long.parseLong(windowTimeOut));
            }

            String windowMonitorInterval = esme.getAttributeValue("windowMonitorInterval");
            if (windowMonitorInterval != null && !windowMonitorInterval.trim().equals("")) {
                esmeImpl.setWindowMonitorInterval(Long.parseLong(windowMonitorInterval));
            }

            String inactivityMonitor = esme.getAttributeValue("inactivityMonitorTimer");
            if (inactivityMonitor != null && !inactivityMonitor.trim().equals("")) {
                esmeImpl.setInactivityMonitorTimer(Long.parseLong(inactivityMonitor));
            }

            boolean disabled = esme.getAttribute("disabled").getBooleanValue();
            if (disabled) {
                esmeImpl.disable();
            }

            String tps = esme.getAttributeValue("tps");
            if (tps != null && !tps.trim().equals("")) {
                esmeImpl.setTps(Integer.parseInt(tps));
            }

            this.configuredEsme.add(esmeImpl);
        }

        logger.info("Esmes loaded.");
    }

    @Override
    public String displayConfig(String esmeName) throws Exception {
        StringBuilder sb = new StringBuilder();
        if (esmeName != null
                && !esmeName.trim().equals("")) {
            EsmeImpl esme = this.getEsme(esmeName);
            if (esme == null) {
                throw new IOException(String.format("Esme %s not found.", esmeName));
            }

            return esme.toString();
        }

        this.configuredEsme.forEach((esme) -> {
            sb.append(esme.toString()).append("\r\n");
        });
        return sb.toString();
    }

    @Override
    public void disableEsme(String esme) throws Exception {
        EsmeImpl esmeImpl = this.getEsme(esme);
        if (esmeImpl == null) {
            throw new IOException(String.format("Esme %s not found.", esme));
        }
        esmeImpl.disable();
    }

    @Override
    public void enableEsme(String systemId) throws Exception {
        EsmeImpl esmeImpl = this.getEsme(systemId);
        if (esmeImpl == null) {
            throw new IOException(String.format("Esme %s not found.", systemId));
        }
        esmeImpl.enable();
    }

    @Override
    public void disconnectEsme(String systemId) throws Exception {
        EsmeImpl esme = this.getEsme(systemId);
        if (esme == null) {
            throw new IOException(String.format("No ESME with name %s is configured",
                    systemId));
        }

        esme.disconnect();
    }

}
