/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server;

import com.cloudhopper.smpp.SmppServerConfiguration;
import com.cloudhopper.smpp.impl.DefaultSmppServer;
import com.cloudhopper.smpp.type.SmppChannelException;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import com.linkedlogics.smpp.server.esme.EsmeManagement;
import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class SmppServer implements SmppServerMBean {

    private final Logger logger = Logger.getLogger(SmppServer.class);
    private DefaultSmppServer defaultSmppServer;
    //configurable parameters
    private long bindTimeOut = 3000;
    private int port = 2775;
    private int maxConnectionSize = 10;
    private long requestExpiryTimeOut = 30000;
    private long windowMonitorInterval = 15000;
    private int windowSize = 50000;
    private long windowWaitTimeOut = 60000;
    private final EsmeManagement esmeManagement;
    private final String jmxObjectName = "com.linkedlogics.smpp.server:type=Management,name=SmppServer";
    private MBeanServer mbeanServer;
    private final String esmeManagementJmxObjectName = "com.linkedlogics.smpp.server:type=Management,name=EsmeManagement";
    public final AtomicLong uniqueSequenceIdGenerator = new AtomicLong(0);
    private final Random random = new Random();

    public SmppServer() throws RemoteException {
        this.esmeManagement = new EsmeManagement();

        logger.info("Registering Smpp Server in JMX ");
        mbeanServer = ManagementFactory.getPlatformMBeanServer();
        try {
            mbeanServer.registerMBean(this, ObjectName.getInstance(jmxObjectName));

            mbeanServer.registerMBean(this.esmeManagement, ObjectName.getInstance(esmeManagementJmxObjectName));

            logger.info("Smpp Server registered in JMX " + jmxObjectName);
        } catch (MalformedObjectNameException | NullPointerException | InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException ex) {
            logger.error("Error occured while register Smpp Server in JMX", ex);
        }

    }

    @Override
    public void start() throws SmppChannelException, RemoteException {

        logger.info("Starting Smpp Server");
        // for monitoring thread use, it's preferable to create your own
        // instance of an executor and cast it to a ThreadPoolExecutor from
        // Executors.newCachedThreadPool() this permits exposing thinks like
        // executor.getActiveCount() via JMX possible no point renaming the
        // threads in a factory since underlying Netty framework does not easily
        // allow you to customize your thread names
        //this.executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        // to enable automatic expiration of requests, a second scheduled
        // executor
        // is required which is what a monitor task will be executed with - this
        // is probably a thread pool that can be shared with between all client
        // bootstraps
//        this.monitorExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5,
//                new ThreadFactory() {
//            private AtomicInteger sequence = new AtomicInteger(0);
//
//            @Override
//            public Thread newThread(Runnable r) {
//                Thread t = new Thread(r);
//                t.setName("SmppServerSessionWindowMonitorPool-" + sequence.getAndIncrement());
//                return t;
//            }
//        });
        SmppServerConfiguration smppServerConfiguration = new SmppServerConfiguration();

        smppServerConfiguration.setBindTimeout(this.bindTimeOut);

        smppServerConfiguration.setPort(this.port);

        smppServerConfiguration.setMaxConnectionSize(this.maxConnectionSize);

        smppServerConfiguration.setDefaultRequestExpiryTimeout(this.requestExpiryTimeOut);

        smppServerConfiguration.setNonBlockingSocketsEnabled(true);

        smppServerConfiguration.setDefaultWindowMonitorInterval(this.windowMonitorInterval);

        smppServerConfiguration.setDefaultWindowSize(this.windowSize);

        smppServerConfiguration.setDefaultWindowWaitTimeout(this.requestExpiryTimeOut);

        smppServerConfiguration.setJmxEnabled(false);

        this.defaultSmppServer = new DefaultSmppServer(smppServerConfiguration,
                new ServerHandler());

        this.defaultSmppServer.start();

        logger.info("Smpp Server started");
    }

    public String createMessageId() {
        return String.format("%s%s", System.currentTimeMillis(), random.nextLong());
    }

    @Override
    public void stop() {
        logger.info("Stopping Smpp Server");
        defaultSmppServer.stop();
        this.defaultSmppServer.destroy();
        logger.info("SmppServer stopped");
    }

    /**
     * @return the bindTimeOut
     */
    @Override
    public long getBindTimeOut() {
        return bindTimeOut;
    }

    /**
     * @param bindTimeOut the bindTimeOut to set
     */
    @Override
    public void setBindTimeOut(long bindTimeOut) {
        this.bindTimeOut = bindTimeOut;
    }

    /**
     * @return the maxConnectionSize
     */
    @Override
    public int getMaxConnectionSize() {
        return maxConnectionSize;
    }

    /**
     * @param maxConnectionSize the maxConnectionSize to set
     */
    @Override
    public void setMaxConnectionSize(int maxConnectionSize) {
        this.maxConnectionSize = maxConnectionSize;
    }

    /**
     * @return the port
     */
    @Override
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    @Override
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return the defaultRequestExpiryTimeOut
     */
    @Override
    public long getRequestExpiryTimeOut() {
        return requestExpiryTimeOut;
    }

    /**
     * @param defaultRequestExpiryTimeOut the defaultRequestExpiryTimeOut to set
     */
    @Override
    public void setRequestExpiryTimeOut(long defaultRequestExpiryTimeOut) {
        this.requestExpiryTimeOut = defaultRequestExpiryTimeOut;
    }

    /**
     * @return the defaultWindowMonitorInterval
     */
    @Override
    public long getWindowMonitorInterval() {
        return windowMonitorInterval;
    }

    /**
     * @param defaultWindowMonitorInterval the defaultWindowMonitorInterval to
     * set
     */
    @Override
    public void setWindowMonitorInterval(long defaultWindowMonitorInterval) {
        this.windowMonitorInterval = defaultWindowMonitorInterval;
    }

    /**
     * @return the defaultWindowSize
     */
    @Override
    public int getWindowSize() {
        return windowSize;
    }

    /**
     * @param defaultWindowSize the defaultWindowSize to set
     */
    @Override
    public void setWindowSize(int defaultWindowSize) {
        this.windowSize = defaultWindowSize;
    }

    /**
     * @return the defaultWindowWaitTimeOut
     */
    @Override
    public long getWindowWaitTimeOut() {
        return windowWaitTimeOut;
    }

    /**
     * @param defaultWindowWaitTimeOut the defaultWindowWaitTimeOut to set
     */
    @Override
    public void setWindowWaitTimeOut(long defaultWindowWaitTimeOut) {
        this.windowWaitTimeOut = defaultWindowWaitTimeOut;
    }

    /**
     * @return the esmeManagement
     */
    public EsmeManagement getEsmeManagement() {
        return esmeManagement;
    }

    /**
     * @return the jmxObjectName
     */
    public String getJmxObjectName() {
        return jmxObjectName;
    }

    /**
     * @return the esmeManagementJmxObjectName
     */
    public String getEsmeManagementJmxObjectName() {
        return esmeManagementJmxObjectName;
    }

    @Override
    public void restart() throws SmppChannelException, RemoteException {
        this.stop();
        this.start();
    }

}
