/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkedlogics.smpp.server;

import com.cloudhopper.smpp.PduAsyncResponse;
import com.cloudhopper.smpp.pdu.PduRequest;

/**
 *
 * @author eatakishiyev
 */
public interface SmppUser {

    //Response
    public void onSubmitSmResponse(PduAsyncResponse pduAsyncResponse, SessionHandler session);

    public void onDeliverSmResponse(PduAsyncResponse pduAsyncResponse, SessionHandler session);

    public void onDataSmResponse(PduAsyncResponse pduAsyncResponse, SessionHandler session);

    public void onEnquireLinkResponse(PduAsyncResponse pduAsyncResponse, SessionHandler session);

    //Request
    public void onSubmitSmRequest(PduRequest pduRequest, SessionHandler session);

    public void onDeliverSmRequest(PduRequest pduRequest, SessionHandler session);

    public void onDataSmRequest(PduRequest pduRequest, SessionHandler session);

    public void onEnquireLinkRequest(PduRequest pduRequest, SessionHandler session);
    //

    public void onRequestExpired(PduRequest pduRequest, SessionHandler session);

    public void onConnected(String systemId, int id, Long sessionId, SessionHandler customSession);

}
